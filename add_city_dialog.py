import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GdkPixbuf

class AddCityDialog(Gtk.Dialog):
    def __init__(self, parent):
        """This will initialize the dialog box for adding new cities."""

        Gtk.Dialog.__init__(self, "My Dialog", parent)

        self.set_default_size(150, 100)

        # Set main window to be the parent
        self.parent = parent
        self.label = Gtk.Label("Enter the city name")
        add_button = Gtk.Button(label="Add")
        add_button.connect("clicked", self.on_add_button_clicked)
        cancel_button = Gtk.Button(label="Cancel")
        cancel_button.connect("clicked", self.on_cancel_button_clicked)
        self.cityEntry = Gtk.Entry()
        box = self.get_content_area()
        box.add(self.label)
        box.add(self.cityEntry)
        box.add(add_button)
        box.add(cancel_button)
        self.show_all()

    def on_add_button_clicked(self, widget):
        """This will be called after submitting the name of new city."""

        city_name = self.cityEntry.get_text().strip()
        if city_name.upper() in [x[1].upper() for x in self.parent.city_store]:
            self.label.set_text('That city is already in your list...')
        elif city_name == "":
            self.label.set_text('Please enter the city name first...')
        else:
            self.parent.refresh_data(city_name, new=True)
            self.destroy()

    def on_cancel_button_clicked(self, widget):
        """Close dialog box."""
        self.destroy()
