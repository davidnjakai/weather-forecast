import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GdkPixbuf
from apputils import interestedCities, addCity, saveWeather, fetchWeather, saveForecast, fetchForecast, installApp
from env import env
from urllib import request, parse, error
import json
from datetime import datetime
from add_city_dialog import AddCityDialog

class GridWindow(Gtk.Window):

    def __init__(self):
        """This will initialize the main window for the applicaion"""

        self.cache = {
            'icons': {},  # This will store a dictionary with icon_code-pixbuf as the key-value pair
            'weather_statuses': {},
            'forecasts': {}
        }

        installApp()

        cities = interestedCities()

        Gtk.Window.__init__(self, title="Weather Application")

        grid = Gtk.Grid()
        self.add(grid)

        self.current_city_of_interest = cities[0]['city_name']

        self.location_label = Gtk.Label(label=self.current_city_of_interest)

        city_id = cities[0]['city_id']

        current_weather_json = self.weather_loader(self.current_city_of_interest)

        self.cache['weather_statuses'][city_id] = current_weather_json
        saveWeather(city_id, current_weather_json)

        icon_code = current_weather_json['icon']
        new_icon = self.icon_loader(icon_code)
        self.current_weather_icon = Gtk.Image.new_from_pixbuf(new_icon)
        self.current_temperature_label = Gtk.Label(label=str(current_weather_json['temperature']) + 'K')

        #Store list of weather forecast data for the next 24 hours
        forecast_list = self.one_day_forecast(city_id, self.current_city_of_interest)

        saveForecast(city_id, {"forecast": forecast_list})

        hour_counter = 1

        self.forecast_icons = {}
        self.forecast_temperatures = {}
        self.forecast_times = {}

        for weather in forecast_list:
            icon_code = weather['weather'][0]['icon']
            self.forecast_icons['hour_' + str(hour_counter)] = Gtk.Image.new_from_pixbuf(self.icon_loader(icon_code))
            self.forecast_temperatures['hour_' + str(hour_counter)] = Gtk.Label(label=str(weather['main']['temp']) + 'K')
            self.forecast_times['hour_' + str(hour_counter)] = Gtk.Label(label=datetime.strptime(weather['dt_txt'], '%Y-%m-%d %H:%M:%S').strftime('%I %p'))
            hour_counter += 1

        self.city_store = Gtk.ListStore(int, str)

        #Store list of cities to be displayed in combo box
        for city in cities:
            city_id = city['city_id']
            city_name = city['city_name']
            self.city_store.append([city_id, city_name])
            forecast_list = self.one_day_forecast(city_id, city_name)
            saveForecast(city_id, {"forecast": forecast_list})

        city_combo = Gtk.ComboBox.new_with_model_and_entry(self.city_store)
        city_combo.connect("changed", self.on_city_combo_changed)
        city_combo.set_entry_text_column(1)

        add_city_button = Gtk.Button(label="Add new city")
        add_city_button.connect("clicked", self.on_add_city_button_clicked)

        refresh_button = Gtk.Button(label="Refresh")
        refresh_button.connect("clicked", self.on_refresh_button_clicked)

        grid.attach(self.location_label, 0, 0, 5, 1)
        grid.attach(city_combo, 8, 0, 1, 1)
        grid.attach_next_to(add_city_button, city_combo, Gtk.PositionType.RIGHT, 2, 1)
        grid.attach_next_to(self.current_weather_icon, self.location_label, Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.current_temperature_label, self.current_weather_icon, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(self.forecast_temperatures['hour_1'], self.current_weather_icon, Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.forecast_temperatures['hour_2'], self.forecast_temperatures['hour_1'], Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(self.forecast_temperatures['hour_3'], self.forecast_temperatures['hour_2'], Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(self.forecast_temperatures['hour_4'], self.forecast_temperatures['hour_3'], Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(self.forecast_temperatures['hour_5'], self.forecast_temperatures['hour_4'], Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(self.forecast_temperatures['hour_6'], self.forecast_temperatures['hour_5'], Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(self.forecast_temperatures['hour_7'], self.forecast_temperatures['hour_6'], Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(self.forecast_temperatures['hour_8'], self.forecast_temperatures['hour_7'], Gtk.PositionType.RIGHT, 1, 1)

        grid.attach_next_to(self.forecast_icons['hour_1'], self.forecast_temperatures['hour_1'], Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.forecast_icons['hour_2'], self.forecast_temperatures['hour_2'], Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.forecast_icons['hour_3'], self.forecast_temperatures['hour_3'], Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.forecast_icons['hour_4'], self.forecast_temperatures['hour_4'], Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.forecast_icons['hour_5'], self.forecast_temperatures['hour_5'], Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.forecast_icons['hour_6'], self.forecast_temperatures['hour_6'], Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.forecast_icons['hour_7'], self.forecast_temperatures['hour_7'], Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.forecast_icons['hour_8'], self.forecast_temperatures['hour_8'], Gtk.PositionType.BOTTOM, 1, 1)

        grid.attach_next_to(self.forecast_times['hour_1'], self.forecast_icons['hour_1'], Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.forecast_times['hour_2'], self.forecast_icons['hour_2'], Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.forecast_times['hour_3'], self.forecast_icons['hour_3'], Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.forecast_times['hour_4'], self.forecast_icons['hour_4'], Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.forecast_times['hour_5'], self.forecast_icons['hour_5'], Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.forecast_times['hour_6'], self.forecast_icons['hour_6'], Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.forecast_times['hour_7'], self.forecast_icons['hour_7'], Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(self.forecast_times['hour_8'], self.forecast_icons['hour_8'], Gtk.PositionType.BOTTOM, 1, 1)

        grid.attach(refresh_button, 3, 5, 3, 1)



    def on_city_combo_changed(self, combo):
        """This will be called when a different city is selected."""

        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            city_id = model[tree_iter][0]
            city_name = model[tree_iter][1]
            self.update_display(city_id, city_name)
            self.current_city_of_interest = city_name

    def weather_loader(self, city_name):
        """This will fetch the weather data from openweathermap API.

        The resulting data will be cached in the app's cache.

        The data will then be returned as a dictionary.
        """

        query_params = {
            'q': city_name,
            'APPID': env['api_key'],
        }

        query_string = parse.urlencode(query_params)
        url = env['weather_url'] + '?' + query_string
        try:
            response = request.urlopen(url)
        except error.HTTPError:
            return
        weather_json = json.loads(response.read())
        city_id = weather_json['id']
        response.close()

        weather = weather_json['weather'][0]['main']
        weather_description = weather_json['weather'][0]['description']
        temperature = weather_json['main']['temp']
        icon_code = weather_json['weather'][0]['icon']

        cache_dict = {
            'weather': weather,
            'description': weather_description,
            'temperature': temperature,
            'city_id': city_id,
            'date_recorded': datetime.today(),
            'icon': icon_code
        }
        self.cache['weather_statuses'][city_id] = cache_dict
        return cache_dict

    def icon_loader(self, icon_code):
        """This function returns the icon as a Pixbuf object.

        If the Pixbuf is not already cached, it is fetched from openweathermap,
        cached and returned.
        """

        if icon_code in self.cache['icons']:
            return self.cache['icons'][icon_code]
        icon_url = 'http://openweathermap.org/img/w/{icon_code}.png'.format(icon_code = icon_code)
        response = request.urlopen(icon_url)
        loader = GdkPixbuf.PixbufLoader()
        loader.write(response.read())
        loader.close()
        pixbuf = loader.get_pixbuf()
        self.cache['icons'][icon_code] = pixbuf
        return pixbuf

    def one_day_forecast(self, city_id, city_name):
        """"
        This function will return forecast for the next 24 hours using openweathermap api.

        It will return a list of 8 dictionaries (spaced by 3 hours) representing weather status.
        """

        query_params = {
            'q': city_name,
            'APPID': env['api_key']
        }

        query_string = parse.urlencode(query_params)
        url = env['forecast_url'] + '?' + query_string
        response = request.urlopen(url)
        forecast_json = json.loads(response.read())
        response.close()

        #We're discarding the first weather item as it will be historical by between 0 - 3 hours.
        forecast_list = forecast_json['list'][1:9]
        self.cache['forecasts'][city_id] = forecast_list
        return forecast_list

    def on_add_city_button_clicked(self, widget):
        """This will display the dialog box for adding a new city."""

        add_city_dialog = AddCityDialog(self)
        response = add_city_dialog.run()

        if response == Gtk.ResponseType.CANCEL:
            add_city_dialog.destroy()

    def refresh_data(self, city_name, new=False):
        """This will refresh weather data by querying from openweathermap an updating display.

        If new is True, a new city record will be saved to the DB and added to the combo box.
        """

        current_weather_json = self.weather_loader(city_name)

        #Do nothing if the response was invalid
        if current_weather_json is None:
            return
        city_id = current_weather_json['city_id']

        #Persist weather data  to database
        saveWeather(city_id, current_weather_json)
        self.location_label.set_text(city_name)
        forecast_list = self.one_day_forecast(city_id, city_name)
        saveForecast(city_id, {"forecast": forecast_list})

        icon_code = current_weather_json['icon']
        self.current_weather_icon.set_from_pixbuf(self.icon_loader(icon_code))
        self.current_temperature_label.set_text(str(current_weather_json['temperature']) + 'K')

        #Set counter for forecast weather updates
        hour_counter = 1
        for weather in forecast_list:
            icon_code = weather['weather'][0]['icon']
            self.forecast_icons['hour_' + str(hour_counter)].set_from_pixbuf(self.icon_loader(icon_code))
            self.forecast_temperatures['hour_' + str(hour_counter)].set_text(str(weather['main']['temp']) + 'K')
            self.forecast_times['hour_' + str(hour_counter)].set_text(datetime.strptime(weather['dt_txt'], '%Y-%m-%d %H:%M:%S').strftime('%I %p'))
            hour_counter += 1

        if new:
            #Persist new city to database
            addCity(city_id, city_name)

            #Add city to combo box
            self.city_store.append([city_id, city_name])
            self.current_city_of_interest = city_name

    def update_display(self, city_id, city_name):
        """This will be called when the city is changed from the combo box."""

        if city_id in self.cache['weather_statuses']:
            current_weather_json = self.cache['weather_statuses'][city_id]
        else:
            current_weather_json = fetchWeather(city_id)[0]
            self.cache['weather_statuses'][city_id] = current_weather_json
        self.location_label.set_text(city_name)

        if city_id in self.cache['forecasts']:
            forecast_list = self.cache['forecasts'][city_id]
        else:
            forecast_list = fetchForecast(city_id)[0]['forecast']
            self.cache['forecasts'][city_id] = forecast_list

        icon_code = current_weather_json['icon']
        self.current_weather_icon.set_from_pixbuf(self.icon_loader(icon_code))
        self.current_temperature_label.set_text(str(current_weather_json['temperature']) + 'K')

        hour_counter = 1

        for weather in forecast_list:
            icon_code = weather['weather'][0]['icon']
            self.forecast_icons['hour_' + str(hour_counter)].set_from_pixbuf(self.icon_loader(icon_code))
            self.forecast_temperatures['hour_' + str(hour_counter)].set_text(str(weather['main']['temp']) + 'K')
            self.forecast_times['hour_' + str(hour_counter)].set_text(datetime.strptime(weather['dt_txt'], '%Y-%m-%d %H:%M:%S').strftime('%I %p'))
            hour_counter += 1

    def on_refresh_button_clicked(self, widget):
        """This handles the button click event for the Refresh button."""

        self.refresh_data(self.current_city_of_interest)


win = GridWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
