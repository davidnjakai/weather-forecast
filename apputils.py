import sqlite3
import json
from sqlite3 import Error
from datetime import datetime
import pickle

def connect(db_file):
    """Connect to sqlite3 db and return a connection object"""

    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)
    return None


def installApp():
    """Create sqlite3 db file and create tables if they do not exist."""

    db_file_name = 'weather.db'
    db_file_handle = open(db_file_name, 'a')
    db_file_handle.close()

    conn = connect(db_file_name)

    cur = conn.cursor()

    try:
        cur.execute("CREATE TABLE cities (id INT, city_name VARCHAR, interested BOOLEAN DEFAULT 1)")
    except sqlite3.OperationalError:
        print("Table cities already created")
    try:
        cur.execute("CREATE TABLE weather_records (id INTEGER PRIMARY KEY, weather VARCHAR,\
        description VARCHAR, temperature FLOAT, city_id INT, date_recorded DATETIME, icon_code VARCHAR)")
    except sqlite3.OperationalError:
        print("Table weather_records already created")
    try:
        cur.execute("CREATE TABLE forecasts (id INTEGER PRIMARY KEY, forecast_json BLOB, city_id INT)")
    except sqlite3.OperationalError:
        print("Table forecasts already created")
        return

    #Set initial city record when starting up for the first time
    cur.execute("INSERT INTO cities(id, city_name) values(2643743, 'London')")
    conn.commit()
    conn.close()

def interestedCities():
    """Fetch list of saved cities."""

    cities = []

    conn = connect('weather.db')
    cur = conn.cursor()
    result = cur.execute("SELECT * FROM cities WHERE interested = 1")

    for row in result:
        cities.append({'city_id': row[0], 'city_name': row[1]})

    return cities

def addCity(city_id, city_name):
    """Persist new city details to db."""

    data = (city_id, city_name)
    conn = connect('weather.db')
    cur = conn.cursor()
    result = cur.execute("SELECT * FROM cities WHERE id = ? and city_name = ?", data)
    row_count = 0
    for row in result:
        row_count += 1

    if row_count > 0:
        cur.execute("UPDATE cities SET interested = 1 WHERE id = ? and city_name = ?", data)
    else:
        cur.execute("INSERT INTO cities(id, city_name) VALUES(?,?)", data)
    conn.commit()
    conn.close()

def saveWeather(city_id, weather_json):
    """Persist new weather data to db."""

    weather = weather_json['weather']
    description = weather_json['description']
    date_recorded = datetime.today()
    temperature = weather_json['temperature']
    icon_code = weather_json['icon']

    data = (weather, description, temperature, city_id, date_recorded, icon_code)

    conn = connect('weather.db')
    cur = conn.cursor()
    cur.execute('INSERT INTO weather_records(weather, description, temperature, city_id, date_recorded, icon_code)\
    VALUES(?,?,?,?,?,?)', data)
    conn.commit()
    conn.close()

def fetchWeather(city_id):
    """Fetch list of saved weather data for city_id."""

    weather_records = []

    conn = connect('weather.db')
    cur = conn.cursor()
    result = cur.execute("SELECT * FROM weather_records WHERE city_id = ? order by 1 desc limit 10", (city_id,))

    for row in result:
        weather_records.append({'weather': row[1], 'description': row[2], 'temperature': row[3], 'city_id': row[4], 'date_recorded': row[5], 'icon': row[6]})

    conn.close()

    return weather_records

def saveForecast(city_id, forecast_json):
    """Persist new forecast data to db."""

    data = (city_id, pickle.dumps(forecast_json))
    conn = connect('weather.db')
    cur = conn.cursor()
    cur.execute('INSERT INTO forecasts(city_id, forecast_json)\
    VALUES(?,?)', data)
    conn.commit()
    conn.close()

def fetchForecast(city_id):
    """Fetch list of saved forecast data for city_id."""

    forecast_records = []

    conn = connect('weather.db')
    cur = conn.cursor()
    result = cur.execute("SELECT * FROM forecasts WHERE city_id = ? order by 1 desc limit 10", (city_id,))

    for row in result:
        forecast_records.append(pickle.loads(row[1]))

    conn.close()

    return forecast_records
