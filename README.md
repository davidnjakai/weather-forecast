# Weather forecast GTK application
Python GTK weather forecast application syncing data from openweathermap api

##Dependencies
Python 3.7.1

## Installation
Clone the repo from Bitbucket:
```
git clone https://bitbucket.org/davidnjakai/weather-forecast.git
```

Navigate to the root folder:
```
cd weather-forecast
```

Install the required packages:
```
pip install -r requirements.txt
```

Run the application using Python 3.7.1
```
python3 grid.py
```
## Credits

[David Njakai](https://bitbucket.org/davidnjakai/)
